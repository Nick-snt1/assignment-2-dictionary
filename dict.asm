%include "lib.inc"

%define POINTER_SIZE 8

section .text

global find_word

find_word: 
; in: rdi - str_pointer, rsi - last_entry_pointer 
; out: rax = str in dict ? entry_pointer : 0 
.loop:
    push rdi
    push rsi
    add rsi, POINTER_SIZE
    call string_equals 
    pop rsi
    pop rdi

    test rax, rax
    jnz .end

    mov rsi, [rsi]  ; rsi = next entry pointer

    test rsi, rsi
    jnz .loop       ; if reach the end of the dict
    xor rsi, rsi    ; then rsi = 0
.end:               ; else rsi = entry_pointer
    mov rax, rsi
    ret



    