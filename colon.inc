%define HEAD_POINTER 0

%macro colon 2

%2: dq HEAD_POINTER 
    db %1, 0
    
%define HEAD_POINTER %2

%endmacro    