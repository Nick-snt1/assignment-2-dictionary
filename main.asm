%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFF_SIZE 256
%define POINTER_SIZE 8

section .rodata
    entry_msg: db "Enter Radiohead's album name: ", 0
    overflow_msg: db "Entered value is to long!", 0
    not_found_msg: db "No such album, i wish it could exist too :(", 0
    found_msg: db "Go here: ", 0 

section .bss
    buff:   resb BUFF_SIZE

section .text

global _start

_start:
    mov rdi, entry_msg
    call print_string   ;print entry msg

    mov rdi, buff
    mov rsi, BUFF_SIZE  
    call read_word      ; read users input

    push rdx            ; save word length

    test rax, rax       
    jz .overflow

    mov rdi, rax
    mov rsi, HEAD_POINTER
    call find_word      ; try to find key

    test rax, rax       
    jz .not_found

    add rax, POINTER_SIZE
    push rax            ; save value's addr

    mov rdi, found_msg  
    call print_string

    pop rdi
    pop rax             ; get readed word's length
    add rdi, rax        ; skip key + terminator
    inc rdi           
    call print_string   ; print result
    call print_newline
    xor rdi, rdi
    call exit

.overflow:
    mov rdi, overflow_msg
    jmp .end
.not_found:
    mov rdi, not_found_msg
.end:
    call print_string_error
    call print_newline
    mov rdi, 1
    call exit






