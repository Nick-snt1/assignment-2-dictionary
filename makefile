AS=nasm
ASFLAGS=-f elf64 -g
LD=ld

main: main.o dict.o lib.o
	$(LD) -o $@ $^

main.o: main.asm words.inc
	$(AS) $(ASFLAGS) -o $@ $<


%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<

clean:
	rm -f *.o main

.PHONY: clean
